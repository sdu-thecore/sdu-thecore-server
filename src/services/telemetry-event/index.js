const service = require('express').Router();
const {
  listTelemetryEvents,
  createTelemetryEvent,
  readTelemetryEvent,
  updateTelemetryEvent,
  deleteTelemetryEvent,
} = require('./controllers');
const jwtAuthN = require('../../middlewares/authN/jwt');
const userRole = require('../../middlewares/authZ/userRole');
const session = require('../../middlewares/authZ/session');
const filterFields = require('../../middlewares/filterFields');
const requireFields = require('../../middlewares/requireFields');
const trimStrings = require('../../middlewares/trimStrings');

const isManager = userRole(['staff', 'admin']);

module.exports = () => {
  const modelFields = ['timestamp', 'event_type', 'metadata'];
  const update = ['metadata'];

  // read many telemetry events
  service.get('/telemetry-events', listTelemetryEvents);

  // create one telemetry events
  service.post('/telemetry-events', jwtAuthN);
  service.post('/telemetry-events', session(isManager));
  service.post('/telemetry-events', trimStrings);
  service.post('/telemetry-events', filterFields(modelFields));
  service.post('/telemetry-events', requireFields(modelFields));
  service.post('/telemetry-events', createTelemetryEvent);

  // read telemetry event
  service.get('/telemetry-events/:id', readTelemetryEvent);

  // update telemetry event
  service.patch('/telemetry-events/:id', jwtAuthN);
  service.patch('/telemetry-events/:id', session(isManager));
  service.patch('/telemetry-events/:id', trimStrings);
  service.patch('/telemetry-events/:id', filterFields(update));
  service.patch('/telemetry-events/:id', updateTelemetryEvent);

  // delete telemetry event
  service.delete('/telemetry-events/:id', jwtAuthN);
  service.delete('/telemetry-events/:id', isManager);
  service.delete('/telemetry-events/:id', deleteTelemetryEvent);

  return service;
};
