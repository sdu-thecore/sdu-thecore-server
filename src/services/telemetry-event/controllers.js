const asyncMiddleware = require('express-async-handler');
const TelemetryEvent = require('./model');

exports.listTelemetryEvents = asyncMiddleware(async (req, res) => {
  const docs = await TelemetryEvent.find({});

  return res.status(200).json({
    data: docs,
  });
});

exports.createTelemetryEvent = asyncMiddleware(async (req, res) => {
  const doc = await TelemetryEvent.create(req.body);

  return res.status(201).json({
    data: doc,
  });
});

exports.readTelemetryEvent = asyncMiddleware(async (req, res) => {
  const doc = await TelemetryEvent.findById(req.params.id);

  if (!doc) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'telemetry-event',
      }),
    });
  }

  return res.status(200).json({
    data: doc,
  });
});

exports.updateTelemetryEvent = asyncMiddleware(async (req, res) => {
  const options = { new: true };
  const doc = await TelemetryEvent.findByIdAndUpdate(
    req.params.id,
    req.body,
    options
  );

  if (!doc) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'telemetry-event',
      }),
    });
  }

  return res.status(200).json({
    data: doc,
  });
});

exports.deleteTelemetryEvent = asyncMiddleware(async (req, res) => {
  const doc = await TelemetryEvent.findByIdAndRemove(req.params.id);

  if (!doc) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'telemetry-event',
      }),
    });
  }

  return res.status(204).json(null);
});
