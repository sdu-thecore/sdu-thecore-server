const service = require('express').Router();
const {
  readSchema,
  readMany,
  updateOne,
  readOne,
  deleteOne,
} = require('./controllers');
const trimStrings = require('../../middlewares/trimStrings');
const filterFields = require('../../middlewares/filterFields');
const jwtAuthN = require('../../middlewares/authN/jwt');
const userRole = require('../../middlewares/authZ/userRole');
const session = require('../../middlewares/authZ/session');

const isUser = userRole(['user', 'staff', 'supervisor', 'admin']);
const isStaff = userRole(['staff', 'supervisor', 'admin']);
const isAdmin = userRole(['admin']);

module.exports = () => {
  const updateFields = ['role', 'permissions'];

  // read many users
  service.get('/users', jwtAuthN);
  service.get('/users', isStaff);
  service.get('/users', readMany);

  // read user schema
  service.get('/users/schema', readSchema);

  // read one user
  service.get('/users/:userId', jwtAuthN);
  service.get('/users/:userId', session(isUser));
  service.get('/users/:userId', readOne);

  // update one user
  service.patch('/users/:userId', jwtAuthN);
  service.patch('/users/:userId', isAdmin);
  service.patch('/users/:userId', filterFields(updateFields));
  service.patch('/users/:userId', trimStrings);
  service.patch('/users/:userId', updateOne);

  // delete one user
  service.delete('/users/:userId', jwtAuthN);
  service.delete('/users/:userId', deleteOne);

  return service;
};
