const asyncMiddleware = require('express-async-handler');
const { isString } = require('lodash');
const { isMongoId } = require('validator');
const Machine = require('./model');
const Permission = require('../permission/model');
const Node = require('../node/model');
const User = require('../user/model');

exports.readSchema = asyncMiddleware(async (req, res) =>
  res.status(200).json({
    data: Machine.meta,
  })
);

exports.readNew = asyncMiddleware(async (req, res) => {
  const { permission } = req.query;

  if (!isString(permission)) {
    return res.status(400).json({
      error: res.tmf('validation_query_required', {
        field: 'permission',
      }),
    });
  }

  if (!isMongoId(permission)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const existingPermission = await Permission.findById(permission).select({
    _id: 1,
  });
  if (!existingPermission) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'permission',
      }),
    });
  }

  const number = await Machine.getFreeNumber(permission);

  return res.status(200).json({
    data: new Machine({ number, permission }),
  });
});

exports.readMany = asyncMiddleware(async (req, res) => {
  const limit = parseInt(req.query.limit, 10) || 100;
  const offset = parseInt(req.query.offset, 10) || 0;
  const sortOrder = req.query.sort_order || 'asc';
  const sortBy = req.query.sort_by || 'created';
  const allowedSortOrder = ['asc', 'desc'];
  const allowedSortBy = ['created', 'updated'];
  const populate = {
    path: '',
  };

  if (req.query.populate) {
    const fields = req.query.populate.split(',');
    if (fields.find((field) => !Machine.populatable.includes(field))) {
      return res.status(400).json({
        error: res.tmf('validation_query_value_out_of_range_multiple', {
          field: 'populate',
          items: Machine.populatable.join(','),
        }),
      });
    }
    populate.path = fields.join(' ');
  }

  if (sortBy && Array.isArray(sortBy)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'sort_by',
      }),
    });
  }

  if (sortOrder && Array.isArray(sortOrder)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'sort_order',
      }),
    });
  }

  if (sortOrder && !allowedSortOrder.includes(sortOrder)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'sort_order',
        items: allowedSortOrder.join(', '),
      }),
    });
  }

  if (sortBy && !allowedSortBy.includes(sortBy)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'sort_by',
        items: allowedSortBy.join(', '),
      }),
    });
  }

  const options = { sort: { [sortBy]: sortOrder }, offset, limit, populate };
  const { docs, totalDocs, ...metadata } = await Machine.paginate({}, options);

  return res.status(200).json({
    data: docs,
    count: totalDocs,
    ...metadata,
  });
});

exports.readOne = asyncMiddleware(async (req, res) => {
  const { machineId } = req.params;
  const populate = {
    path: '',
  };

  if (req.query.populate) {
    const fields = req.query.populate.split(',');
    if (fields.find((field) => !Machine.populatable.includes(field))) {
      return res.status(400).json({
        error: res.tmf('validation_query_value_out_of_range_multiple', {
          field: 'populate',
          items: Machine.populatable.join(','),
        }),
      });
    }
    populate.path = fields.join(' ');
  }

  if (!isMongoId(machineId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const machine = await Machine.findById(machineId).populate(populate);
  if (!machine) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'machine',
      }),
    });
  }

  return res.status(200).json({
    data: machine,
  });
});

exports.createOne = asyncMiddleware(async (req, res) => {
  const { permission } = req.body;

  // validate permission
  if (!isString(permission)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'permission',
        type: 'string',
      }),
    });
  }

  if (!isMongoId(permission)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'permission',
        type: 'ObjectId',
      }),
    });
  }

  const existingPermission = await Permission.findById(permission).select({
    _id: 1,
  });
  if (!existingPermission) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'permission',
      }),
    });
  }

  const number = await Machine.getFreeNumber(permission);

  const { status } = req.body;
  if (status) {
    if (!isString(status)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'status',
          type: 'string',
        }),
      });
    }

    if (!Machine.meta.status.enum.includes(status)) {
      return res.status(400).json({
        error: res.tmf('validation_field_value_out_of_range', {
          field: 'status',
          items: Machine.meta.status.enum.join(', '),
        }),
      });
    }
  }

  const { node } = req.body;
  if (node) {
    if (!isString(node)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'node',
          type: 'string',
        }),
      });
    }

    if (!isMongoId(node)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'node',
          type: 'ObjectId',
        }),
      });
    }

    const existingNode = await Node.findById(node);
    if (!existingNode) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'node',
        }),
      });
    }
  }

  const machine = await Machine.create({
    permission,
    number,
    node,
  });

  if (node) {
    await Node.findByIdAndUpdate(node, { machine: machine.id });
  }

  return res.status(201).json({ data: machine });
});

exports.updateOne = asyncMiddleware(async (req, res) => {
  const { machineId } = req.params;

  if (!isMongoId(machineId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const existingMachine = await Machine.findById(machineId);
  if (!existingMachine) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'machine',
      }),
    });
  }

  if (req.user.node && req.user.node.id) {
    if (existingMachine.node.toString() !== req.user.node.id) {
      return res.status(403).json({
        error: res.tmf('auth_error_credentials_required'),
      });
    }
  }

  let { number } = existingMachine;
  const { permission } = req.body;
  if (permission) {
    if (!isString(permission)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'permission',
          type: 'string',
        }),
      });
    }

    if (!isMongoId(permission)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'permission',
          type: 'ObjectId',
        }),
      });
    }

    const existingPermission = await Permission.findById(permission).select({
      _id: 1,
    });
    if (!existingPermission) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'permission',
        }),
      });
    }

    if (existingPermission.id !== permission) {
      number = await Machine.getFreeNumber(permission);
    }
  }

  const { status } = req.body;
  if (status) {
    if (!isString(status)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'status',
          type: 'string',
        }),
      });
    }

    if (!Machine.meta.status.enum.includes(status)) {
      return res.status(400).json({
        error: res.tmf('validation_field_value_out_of_range', {
          field: 'status',
          items: Machine.meta.status.enum.join(', '),
        }),
      });
    }
  }

  const { node } = req.body;
  if (node) {
    if (!isString(node)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'node',
          type: 'string',
        }),
      });
    }

    if (!isMongoId(node)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'node',
          type: 'ObjectId',
        }),
      });
    }

    const existingNode = await Node.findById(node);
    if (!existingNode) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'node',
        }),
      });
    }

    if (existingNode.machine) {
      return res.status(409).json({
        error: res.tmf('error_entity_in_use', {
          entity: 'node',
          resource: 'machine',
        }),
      });
    }
  }

  const { user } = req.body;
  if (user) {
    if (!isString(user)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'user',
          type: 'string',
        }),
      });
    }

    if (!isMongoId(user)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'user',
          type: 'ObjectId',
        }),
      });
    }

    const existingUser = await User.findById(user);
    if (!existingUser) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'user',
        }),
      });
    }
  }

  const update = {
    ...req.body,
    user:
      status === 'busy' || (!status && existingMachine.status === 'busy')
        ? user
        : null,
    number,
  };
  const options = { new: true };
  const machine = await Machine.findByIdAndUpdate(machineId, update, options);

  if (machine.node) {
    await Node.findByIdAndUpdate(machine.node.toString(), {
      machine: machine.id,
    });
  } else {
    await Node.findOneAndUpdate({ machine: machine.id }, { machine: null });
  }

  return res.status(200).json({ data: machine });
});

exports.deleteOne = asyncMiddleware(async (req, res) => {
  const { machineId } = req.params;

  if (!isMongoId(machineId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const machine = await Machine.findByIdAndDelete(machineId);
  if (!machine) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'machine',
      }),
    });
  }

  if (machine.node) {
    await Node.findOneAndUpdate({ machine: machine.id }, { machine: null });
  }

  return res.status(204).json(null);
});
