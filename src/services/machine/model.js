const { times } = require('lodash');
const mongoose = require('../../common/mongoose');
const generateMetaSchema = require('../../common/generateMetaSchema');

const { Schema } = mongoose;
const { ObjectId, Mixed } = Schema.Types;

const status = ['maintenance', 'ready', 'busy'];
const defaultState = status[0];
const sortable = ['permission', 'number', 'status', 'node', 'job'];
const populatable = ['permission', 'node', 'user'];

const machineSchema = new Schema({
  permission: {
    type: ObjectId,
    ref: 'permission',
    required: true,
    unique: false,
    default: null,
  },
  number: {
    type: Number,
    required: false,
    unique: false,
  },
  status: {
    type: String,
    required: false,
    unique: false,
    enum: status,
    default: defaultState,
  },
  attributes: [
    {
      name: {
        type: String,
        required: true,
        unique: false,
      },
      type: {
        type: String,
        required: true,
        unique: false,
      },
      value: {
        type: Mixed,
        required: true,
        unique: false,
      },
      required: false,
      unique: false,
      default: [],
    },
  ],
  node: {
    type: ObjectId,
    ref: 'node',
    required: false,
    unique: false,
    default: null,
  },
  user: {
    type: ObjectId,
    ref: 'user',
    required: false,
    unique: false,
    default: null,
  },
  job: {
    type: ObjectId,
    ref: 'job',
    required: false,
    unique: false,
    default: null,
  },
});

machineSchema.statics.status = status;
machineSchema.statics.populatable = populatable;
machineSchema.statics.sortable = sortable;
machineSchema.statics.meta = generateMetaSchema(machineSchema);

machineSchema.statics.getFreeNumber = async function getFreeNumber(permission) {
  const highestMachine = await this.findOne({ permission })
    .select({ id: 1, number: 1 })
    .sort({ number: -1 });

  let number = 1;
  const availableNumbers = [];

  if (highestMachine) {
    const potentialNumbers = times(highestMachine.number - 1, (i) => i + 1);
    const promises = potentialNumbers.map(async (index) => {
      const specificMachine = await this.findOne({
        permission,
        number: index,
      }).select({ id: 1, number: 1 });
      if (!specificMachine) {
        availableNumbers.push(index);
      }
    });
    await Promise.all(promises);
    if (availableNumbers.length) {
      number = availableNumbers.sort().shift();
    } else {
      number = highestMachine.number + 1;
    }
  }
  return number;
};

module.exports = mongoose.model('machine', machineSchema);
