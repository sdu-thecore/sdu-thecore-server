const mongoose = require('../../common/mongoose');
const generateMetaSchema = require('../../common/generateMetaSchema');

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const sessionSchema = new Schema({
  token: {
    type: String,
    required: false,
    unique: false,
    default: '',
  },
  authorized: {
    type: Boolean,
    required: false,
    unique: false,
    default: false,
  },
  identify: {
    type: Boolean,
    required: false,
    unique: false,
    default: false,
  },
  seen: {
    type: Date,
    required: false,
    unique: false,
    default: Date.now,
  },
  node: {
    type: ObjectId,
    ref: 'node',
    required: false,
    unique: false,
    default: null,
  },
});

sessionSchema.methods.deleteOther = async function deleteOtherMethod() {
  const { _id, node } = this;
  const Model = this.model('session');
  const query = {
    $and: [{ _id: { $ne: _id } }, { node: { $eq: node } }],
  };
  return Model.deleteMany(query);
};

sessionSchema.methods.unidentifyOther = async function unidentifyOther() {
  const { _id, node } = this;
  const Model = this.model('session');
  const query = {
    $and: [
      { _id: { $ne: _id } },
      { node: { $eq: node } },
      { identify: { $eq: true } },
    ],
  };
  return Model.updateMany(query, { identify: false });
};

sessionSchema.statics.meta = generateMetaSchema(sessionSchema);

module.exports = mongoose.model('session', sessionSchema);
