const service = require('express').Router();
const {
  createOne,
  readOne,
  deleteOne,
  readOneContent,
} = require('./controllers');
const jwtAuthN = require('../../middlewares/authN/jwt');
const userRole = require('../../middlewares/authZ/userRole');

const isManager = userRole(['staff', 'admin']);

module.exports = () => {
  // create one image
  service.post('/images', jwtAuthN);
  service.post('/images', isManager);
  service.post('/images', createOne);

  // read one image
  service.get('/images/:imageId', readOne);

  // read metadata of one image
  service.get('/images/:imageId/content', readOneContent);

  // delete one image
  service.delete('/images/:imageId', jwtAuthN);
  service.delete('/images/:imageId', isManager);
  service.delete('/images/:imageId', deleteOne);

  return service;
};
