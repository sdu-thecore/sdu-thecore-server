const asyncMiddleware = require('express-async-handler');
const { isString, isNumber } = require('lodash');
const { isMongoId } = require('validator');
const Job = require('./model');
const User = require('../user/model');
const Machine = require('../machine/model');
const Permission = require('../permission/model');
const Attachment = require('../attachment/model');

exports.readSchema = asyncMiddleware(async (req, res) =>
  res.status(200).json({
    data: Job.meta,
  })
);

exports.readMany = asyncMiddleware(async (req, res) => {
  const limit = parseInt(req.query.limit, 10) || 100;
  const offset = parseInt(req.query.offset, 10) || 0;
  const sortOrder = req.query.sort_order || 'asc';
  const sortBy = req.query.sort_by || 'created';
  const status = req.query.status || undefined;
  const allowedSortOrder = ['asc', 'desc'];
  const allowedSortBy = ['created', 'updated'];
  const allowedStatus = Job.status;
  const populate = { path: '' };
  const query = {};
  const readAllRoles = ['admin', 'staff'];

  const user = await User.findById(req.user.user.id);
  // Check if user has admin or staff privileges.
  if (!readAllRoles.includes(user.role)) {
    // Only expose own jobs to user.
    query.$or = [{ users: { $in: [req.user.user.id], status } }];
    // Allow supervisors to see all jobs, that are assigned to them.
    if (user.role === 'supervisor') {
      query.$or.push({ supervisors: { $in: [req.user.user.id], status } });
    }
  }

  if (req.query.populate) {
    const fields = req.query.populate.split(',');
    if (fields.find((field) => !Job.populatable.includes(field))) {
      return res.status(400).json({
        error: res.tmf('validation_query_value_out_of_range_multiple', {
          field: 'populate',
          items: Job.populatable.join(','),
        }),
      });
    }
    populate.path = fields.join(' ');
  }

  if (status && Array.isArray(status)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'status',
      }),
    });
  }

  if (sortBy && Array.isArray(sortBy)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'sort_by',
      }),
    });
  }

  if (sortOrder && Array.isArray(sortOrder)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'sort_order',
      }),
    });
  }

  if (status && !allowedStatus.includes(status)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'status',
        items: allowedSortOrder.join(', '),
      }),
    });
  }

  if (sortOrder && !allowedSortOrder.includes(sortOrder)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'sort_order',
        items: allowedSortOrder.join(', '),
      }),
    });
  }

  if (sortBy && !allowedSortBy.includes(sortBy)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'sort_by',
        items: allowedSortBy.join(', '),
      }),
    });
  }

  const options = { sort: { [sortBy]: sortOrder }, offset, limit, populate };
  const { docs, totalDocs, ...metadata } = await Job.paginate(query, options);

  return res.status(200).json({
    data: docs,
    count: totalDocs,
    ...metadata,
  });
});

exports.readOne = asyncMiddleware(async (req, res) => {
  const { jobId } = req.params;
  const populate = {
    path: '',
  };

  if (req.query.populate) {
    const fields = req.query.populate.split(',');
    if (fields.find((field) => !Job.populatable.includes(field))) {
      return res.status(400).json({
        error: res.tmf('validation_query_value_out_of_range_multiple', {
          field: 'populate',
          items: Job.populatable.join(','),
        }),
      });
    }
    populate.path = fields.join(' ');
  }

  if (!isMongoId(jobId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const job = await Job.findById(jobId).populate(populate);

  // Ensure that jobs are only visible to involved supervisors and users.
  if (
    req.user.user &&
    (req.user.user.role === 'user' || req.user.user.role === 'supervisor')
  ) {
    if (
      !(
        job.users.find((u) => u.toString() === req.user.user.id) ||
        job.supervisors.find((s) => s.toString() === req.user.user.id)
      )
    ) {
      return res.status(403).json({
        error: res.tmf('auth_error_role_required', {
          items: ['staff', 'admin'].join(', '),
        }),
      });
    }
  }

  if (!job) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'job',
      }),
    });
  }

  return res.status(200).json({
    data: job,
  });
});

exports.createOne = asyncMiddleware(async (req, res) => {
  const {
    permission,
    attachments,
    users,
    supervisors,
    machine,
    description,
    started,
    time,
  } = req.body;

  // Configure status.
  let status = 'pending';
  const autoApprovalRoles = ['staff', 'supervisor', 'admin'];
  if (autoApprovalRoles.includes(req.user.user.role)) {
    status = 'approved';
  }

  // Validate attachments.
  if (
    !Array.isArray(attachments) ||
    !attachments.length ||
    !!attachments.find((a) => !isString(a) || !isMongoId(a))
  ) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'attachments',
        type: 'array of ObjectId',
      }),
    });
  }

  const existingAttachments = await Attachment.find(attachments);
  if (existingAttachments.length !== attachments.length) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'attachment',
      }),
    });
  }

  // Validate permission.
  if (!isString(permission)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'permission',
        type: 'string',
      }),
    });
  }

  if (!isMongoId(permission)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'permission',
        type: 'ObjectId',
      }),
    });
  }

  const existingPermission = await Permission.findById(permission).select({
    _id: 1,
  });
  if (!existingPermission) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'permission',
      }),
    });
  }

  // Validate users.
  if (
    !Array.isArray(users) ||
    !!users.find((u) => !isString(u) || !isMongoId(u))
  ) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'users',
        type: 'array of ObjectId',
      }),
    });
  }

  // Check if user is a human.
  if (req.user && req.user.user && req.user.user.id) {
    // Check if the user is included in the users array.
    if (!users.includes(req.user.user.id)) {
      users.push(req.user.user.id);
    }
  }

  if (!users.length) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'users',
        type: 'array of ObjectId',
      }),
    });
  }

  const existingUsers = await User.find({
    _id: { $in: users },
  });
  if (existingUsers.length !== users.length) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'user',
      }),
    });
  }

  // Validate supervisors.
  if (
    !Array.isArray(supervisors) ||
    !supervisors.length ||
    !!supervisors.find((s) => !isString(s) || !isMongoId(s))
  ) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'supervisors',
        type: 'array of ObjectId',
      }),
    });
  }

  const existingSupervisors = await User.find({
    _id: { $in: supervisors },
  });
  if (existingSupervisors.length !== supervisors.length) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'supervisor',
      }),
    });
  }

  // Validate machine, which is optional.
  if (machine) {
    if (!isString(machine)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'machine',
          type: 'string',
        }),
      });
    }

    if (!isMongoId(machine)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'machine',
          type: 'ObjectId',
        }),
      });
    }

    const existingMachine = await Machine.findById(machine).select({
      _id: 1,
    });
    if (!existingMachine) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'permission',
        }),
      });
    }
  }

  // Validate started.
  if (!(started === null || started !== undefined)) {
    return res.status(404).json({
      error: res.tmf('validation_field_not_editable', {
        field: 'started',
      }),
    });
  }

  // Validate description.
  if (!isString(description)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'description',
        type: 'string',
      }),
    });
  }

  if (description.length < 50) {
    return res.status(400).json({
      error: res.tmf('validation_field_amount_min', {
        field: 'description',
        items: res.tn('%d character', 50),
      }),
    });
  }

  // Validate time.
  if (!isNumber(time)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'time',
        type: 'number',
      }),
    });
  }

  const job = await Job.create({
    status,
    attachments,
    permission,
    users,
    supervisors,
    machine,
    started,
    description,
    time,
  });

  return res.status(201).json({ data: job });
});

exports.updateOne = asyncMiddleware(async (req, res) => {
  const { jobId } = req.params;
  if (!isMongoId(jobId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  let { status, attachments } = req.body;
  const {
    permission,
    users,
    supervisors,
    machine,
    description,
    started,
    time,
  } = req.body;

  if (req.user.user) {
    // Ensure that jobs are only visible to involved supervisors and users.
    const job = await Job.findById(jobId);

    if (!job) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'job',
        }),
      });
    }

    if (req.user.user.role === 'user' || req.user.user.role === 'supervisor') {
      if (
        !(
          job.users.find((u) => u.toString() === req.user.user.id) ||
          job.supervisors.find((s) => s.toString() === req.user.user.id)
        )
      ) {
        return res.status(403).json({
          error: res.tmf('auth_error_role_required', {
            items: ['staff', 'admin'].join(', '),
          }),
        });
      }
    }

    // Ensure that the job status is not editable by users.
    if (req.user.user.role === 'user') {
      // Only allow to resubmit rejected jobs.
      if (job.status === 'rejected') {
        status = 'pending';
      } else {
        status = job.status;
      }
    }
  }

  // Validate attachments.
  if (attachments) {
    if (
      !Array.isArray(attachments) ||
      !attachments.length ||
      !!attachments.find((a) => !isString(a) || !isMongoId(a))
    ) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'attachments',
          type: 'array of ObjectId',
        }),
      });
    }

    const existingAttachments = await Attachment.find(attachments);
    if (existingAttachments.length !== attachments.length) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'attachment',
        }),
      });
    }
  }

  // Validate permission.
  if (permission) {
    if (!isString(permission)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'permission',
          type: 'string',
        }),
      });
    }

    if (!isMongoId(permission)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'permission',
          type: 'ObjectId',
        }),
      });
    }

    const existingPermission = await Permission.findById(permission).select({
      _id: 1,
    });
    if (!existingPermission) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'permission',
        }),
      });
    }
  }

  // Validate users.
  if (users) {
    if (
      !Array.isArray(users) ||
      !!users.find((u) => !isString(u) || !isMongoId(u))
    ) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'users',
          type: 'array of ObjectId',
        }),
      });
    }

    // Check if user is a human.
    if (req.user && req.user.user && req.user.user.id) {
      // Check if the user is included in the users array.
      if (!users.includes(req.user.user.id)) {
        users.push(req.user.user.id);
      }
    }

    if (!users.length) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'users',
          type: 'array of ObjectId',
        }),
      });
    }

    const existingUsers = await User.find({
      _id: { $in: users },
    });
    if (existingUsers.length !== users.length) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'user',
        }),
      });
    }
  }

  // Validate supervisors.
  if (supervisors) {
    if (
      !Array.isArray(supervisors) ||
      !supervisors.length ||
      !!supervisors.find((s) => !isString(s) || !isMongoId(s))
    ) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'supervisors',
          type: 'array of ObjectId',
        }),
      });
    }

    const existingSupervisors = await User.find({
      _id: { $in: supervisors },
    });
    if (existingSupervisors.length !== supervisors.length) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'supervisor',
        }),
      });
    }
  }

  // Validate machine, which is optional.
  if (machine) {
    if (!isString(machine)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'machine',
          type: 'string',
        }),
      });
    }

    if (!isMongoId(machine)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'machine',
          type: 'ObjectId',
        }),
      });
    }

    const existingMachine = await Machine.findById(machine).select({
      _id: 1,
    });
    if (!existingMachine) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'permission',
        }),
      });
    }
  }

  // Validate started.
  if (started) {
    // Ensure that the supplied date is valid.
    const startTimestamp = Date.parse(started);
    if (Number.isNaN(startTimestamp)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'started',
          type: 'date',
        }),
      });
    }

    // Ensure that the supplied date is not in the future.
    const timestamp = Date.now();
    if (timestamp < startTimestamp) {
      return res.status(400).json({
        error: res.tmf('validation_field_invalid', {
          field: 'started',
        }),
      });
    }
  }

  // Validate description.
  if (description) {
    if (!isString(description)) {
      return res.status(400).json({
        error: res.tmf('validation_field_type_invalid', {
          field: 'description',
          type: 'string',
        }),
      });
    }

    if (description.length < 50) {
      return res.status(400).json({
        error: res.tmf('validation_field_amount_min', {
          field: 'description',
          items: res.tn('%d character', 50),
        }),
      });
    }
  }

  // Validate time.
  if (time) {
    if (!isNumber(time)) {
      return res.status(400).json({
        error: res.tmf('validation_field_invalid', {
          field: 'time',
        }),
      });
    }

    if (req.user.user && req.user.user.role === 'user') {
      status = 'pending';
    }
  }

  // Clean up attachments of expired jobs.
  if (status === 'expired') {
    const job = await Job.findById(jobId);

    const promises = job.attachments.map((attachment) =>
      Attachment.findByIdAndDelete(attachment)
    );

    await Promise.all(promises);

    attachments = [];
  }

  const options = { new: true };
  const update = {
    status,
    attachments,
    permission,
    users,
    supervisors,
    machine,
    started,
    description,
    time,
  };
  Object.keys(update).forEach((key) => {
    if (!update[key]) {
      delete update[key];
    }
  });
  const job = await Job.findByIdAndUpdate(jobId, update, options);

  return res.status(200).json({ data: job });
});

exports.deleteOne = asyncMiddleware(async (req, res) => {
  const { jobId } = req.params;

  if (!isMongoId(jobId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  if (req.user.user) {
    // Ensure that jobs are only visible to involved supervisors and users.
    const job = await Job.findById(jobId);

    if (!job) {
      return res.status(404).json({
        error: res.tmf('error_entity_not_found', {
          entity: 'job',
        }),
      });
    }

    if (req.user.user.role === 'user' || req.user.user.role === 'supervisor') {
      if (
        !(
          job.users.find((u) => u.toString() === req.user.user.id) ||
          job.supervisors.find((s) => s.toString() === req.user.user.id)
        )
      ) {
        return res.status(403).json({
          error: res.tmf('auth_error_role_required', {
            items: ['staff', 'admin'].join(', '),
          }),
        });
      }
    }
  }

  const job = await Job.findByIdAndDelete(jobId);
  if (!job) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'job',
      }),
    });
  }

  return res.status(204).json(null);
});
