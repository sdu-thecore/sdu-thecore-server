const asyncMiddleware = require('express-async-handler');
const yup = require('yup');
const { stripLow, isMongoId } = require('validator');
const readManyEndpoint = require('../../common/endpoints/readMany');
const readSchemaEndpoint = require('../../common/endpoints/readSchema');
const readOneEndpoint = require('../../common/endpoints/readOne');
const deleteOneEndpoint = require('../../common/endpoints/deleteOne');
const {
  ConflictError,
  BodyValidationError,
  ParameterValidationError,
  EntityNotFoundError,
} = require('../../common/errors');
const Image = require('../image/model');
const ToolType = require('./model');

const sanitize = (value) => (value ? stripLow(value, true) : value);
const validate = async (schema, payload) => {
  try {
    await schema.validate(payload, {
      strict: true,
      abortEarly: true,
      stripUnknown: true,
    });
  } catch (err) {
    if (err.name === 'ValidationError') {
      throw new BodyValidationError(err.errors.shift());
    }
    throw err;
  }
};

exports.readSchema = readSchemaEndpoint(ToolType);

exports.readMany = readManyEndpoint(ToolType);

exports.createOne = asyncMiddleware(async (req, res) => {
  const schema = yup
    .object({
      name: yup
        .string()
        .ensure()
        .trim()
        .transform(sanitize)
        .min(
          2,
          res.tmf('validation_field_amount_min', {
            field: 'name',
            items: res.tn('%d character', 2),
          })
        )
        .max(
          250,
          res.tmf('validation_field_amount_max', {
            field: 'name',
            items: res.tn('%d character', 250),
          })
        )
        .required(
          res.tmf('validation_field_required', {
            field: 'name',
          })
        ),
      image: yup
        .string()
        .ensure()
        .trim()
        .test({
          name: 'mongoId',
          exclusive: false,
          message: res.tmf('validation_field_type_invalid', {
            field: 'image',
            type: 'ObjectId',
          }),
          test: (value) =>
            value === null || value === undefined || isMongoId(value),
        })
        .required(),
      description: yup
        .string()
        .ensure()
        .trim()
        .transform(sanitize)
        .max(
          250,
          res.tmf('validation_field_amount_max', {
            field: 'description',
            items: res.tn('%d character', 250),
          })
        )
        .notRequired()
        .default(''),
    })
    .noUnknown();

  const payload = schema.cast(req.body);

  await validate(schema, payload);

  const existingWithName = await ToolType.findOne({ name: payload.name });
  if (existingWithName) {
    throw new ConflictError(
      res.tmf('validation_entity_exists', {
        model: 'tool_type',
        uniqueProperties: 'name',
      })
    );
  }

  const image = await Image.findById(payload.image);
  if (!image) {
    throw new EntityNotFoundError(
      res.tmf('error_entity_not_found', {
        entity: 'image',
      })
    );
  }

  const toolType = await ToolType.create(payload);

  return res.status(201).json({
    data: toolType,
  });
});

exports.readOne = readOneEndpoint(ToolType);

exports.updateOne = asyncMiddleware(async (req, res) => {
  const { toolTypeId } = req.params;

  if (!isMongoId(toolTypeId)) {
    throw new ParameterValidationError(
      res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      })
    );
  }

  const schema = yup
    .object({
      name: yup
        .string()
        .ensure()
        .trim()
        .transform(sanitize)
        .min(
          2,
          res.tmf('validation_field_amount_min', {
            field: 'name',
            items: res.tn('%d character', 2),
          })
        )
        .max(
          250,
          res.tmf('validation_field_amount_max', {
            field: 'name',
            items: res.tn('%d character', 250),
          })
        )
        .notRequired()
        .default(undefined),
      image: yup
        .string()
        .ensure()
        .trim()
        .test({
          name: 'mongoId',
          exclusive: false,
          message: res.tmf('validation_field_type_invalid', {
            field: 'image',
            type: 'ObjectId',
          }),
          test: (value) =>
            value === null || value === undefined || isMongoId(value),
        })
        .notRequired()
        .default(undefined),
      description: yup
        .string()
        .ensure()
        .trim()
        .transform(sanitize)
        .max(
          250,
          res.tmf('validation_field_amount_max', {
            field: 'description',
            items: res.tn('%d character', 250),
          })
        )
        .notRequired()
        .default(undefined),
    })
    .noUnknown();

  const payload = schema.cast(req.body);

  await validate(schema, payload);

  const nameQuery = { _id: { $ne: toolTypeId }, name: payload.name };
  const existingWithName = await ToolType.findOne(nameQuery);
  if (existingWithName) {
    throw new ConflictError(
      res.tmf('validation_entity_exists', {
        model: 'tool_type',
        uniqueProperties: 'name',
      })
    );
  }

  const image = await Image.findById(payload.image);
  if (!image) {
    throw new EntityNotFoundError(
      res.tmf('error_entity_not_found', {
        entity: 'image',
      })
    );
  }

  const options = { new: true };
  const toolType = await ToolType.findByIdAndUpdate(
    toolTypeId,
    payload,
    options
  );
  if (!toolType) {
    throw new EntityNotFoundError(
      res.tmf('error_entity_not_found', {
        type: 'ObjectId',
      })
    );
  }

  return res.status(200).json({ data: toolType });
});

exports.deleteOne = deleteOneEndpoint(ToolType);
