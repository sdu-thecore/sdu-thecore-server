const asyncMiddleware = require('express-async-handler');
const { NotFoundError } = require('../../common/errors');
const Node = require('../node/model');

exports.readOwn = asyncMiddleware(async (req, res) => {
  if (req.user) {
    if (req.user.node) {
      const node = await Node.findById(req.user.node.id);
      if (node) {
        return res.status(200).json({
          data: node,
          model: 'node',
        });
      }
    }

    if (req.user.user && req.user.user.email) {
      return res.status(200).json({
        data: req.user.user,
        model: 'user',
      });
    }
  }

  throw new NotFoundError(
    res.tmf('error_entity_not_found', {
      entity: 'identity',
    })
  );
});
