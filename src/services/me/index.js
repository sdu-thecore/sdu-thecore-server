const service = require('express').Router();
const { readOwn } = require('./controllers');
const jwtAuthN = require('../../middlewares/authN/jwt');

module.exports = () => {
  // read own user
  service.get('/me', jwtAuthN);
  service.get('/me', readOwn);

  return service;
};
