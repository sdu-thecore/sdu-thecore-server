const asyncMiddleware = require('express-async-handler');
const { get, isString, isBoolean, isUndefined } = require('lodash');
const { isAlpha, isAlphanumeric, isMongoId } = require('validator');
const Permission = require('./model');
const User = require('../user/model');
const Image = require('../image/model');

exports.readSchema = asyncMiddleware(async (req, res) =>
  res.status(200).json({
    data: Permission.meta,
  })
);

exports.readMany = asyncMiddleware(async (req, res) => {
  const limit = parseInt(req.query.limit, 10) || 100;
  const offset = parseInt(req.query.offset, 10) || 0;
  const sortOrder = req.query.sort_order || 'asc';
  const sortBy = req.query.sort_by || 'created';
  const allowedSortOrder = ['asc', 'desc'];
  const allowedSortBy = ['created', 'updated'];

  if (sortBy && Array.isArray(sortBy)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'sort_by',
      }),
    });
  }

  if (sortOrder && Array.isArray(sortOrder)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'sort_order',
      }),
    });
  }

  if (sortOrder && !allowedSortOrder.includes(sortOrder)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'sort_order',
        items: allowedSortOrder.join(', '),
      }),
    });
  }

  if (sortBy && !allowedSortBy.includes(sortBy)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'sort_by',
        items: allowedSortBy.join(', '),
      }),
    });
  }

  const options = { sort: { [sortBy]: sortOrder }, offset, limit };
  const { docs, totalDocs, ...metadata } = await Permission.paginate(
    {},
    options
  );

  return res.status(200).json({
    data: docs,
    count: totalDocs,
    ...metadata,
  });
});

exports.createOne = asyncMiddleware(async (req, res) => {
  // verify abbreviation
  if (!isString(req.body.abbreviation)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'abbreviation',
        type: 'string',
      }),
    });
  }

  if (!req.body.abbreviation) {
    return res.status(400).json({
      error: res.tmf('validation_field_required', {
        field: 'abbreviation',
      }),
    });
  }

  const abbreviation = req.body.abbreviation.toUpperCase();
  if (abbreviation.length !== 3) {
    return res.status(400).json({
      error: res.tmf('validation_field_amount', {
        field: 'abbreviation',
        items: res.tn('%d character'),
      }),
    });
  }

  if (!isAlpha(abbreviation)) {
    return res.status(400).json({
      error: res.tmf('validation_field_characters_allowed', {
        field: 'abbreviation',
        items: res.tn('letters'),
      }),
    });
  }

  const existingWithAbbreviation = await Permission.findOne({ abbreviation });
  if (existingWithAbbreviation) {
    return res.status(409).json({
      error: res.tmf('validation_entity_exists', {
        model: 'permission',
        uniqueProperties: 'abbreviation',
      }),
    });
  }

  // verify name
  if (!isString(req.body.name)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'name',
        type: 'string',
      }),
    });
  }

  if (!req.body.name) {
    return res.status(400).json({
      error: res.tmf('validation_field_required', {
        field: 'name',
      }),
    });
  }

  const name = req.body.name.split(/\s+/g).join(' ');
  if (!isAlphanumeric(name.split(' ').join(''))) {
    return res.status(400).json({
      error: res.tmf('validation_field_characters_allowed', {
        field: 'name',
        items: res.t('letters, numbers and spaces'),
      }),
    });
  }

  const existingWithName = await Permission.findOne({ name });
  if (existingWithName) {
    return res.status(409).json({
      error: res.tmf('validation_entity_exists', {
        model: 'permission',
        uniqueProperty: 'name',
      }),
    });
  }

  // verify description
  const { description } = req.body;
  if (description && !isString(description)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'description',
        type: 'string',
      }),
    });
  }

  // verify scheduling
  const { scheduling } = req.body;
  if (!isString(scheduling)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'scheduling',
        type: 'string',
      }),
    });
  }

  if (!scheduling) {
    return res.status(400).json({
      error: res.tmf('validation_field_required', {
        field: 'scheduling',
      }),
    });
  }

  if (!Permission.meta.scheduling.enum.includes(scheduling)) {
    return res.status(400).json({
      error: res.tmf('validation_field_value_out_of_range', {
        field: 'scheduling',
        items: Permission.meta.scheduling.enum.join(','),
      }),
    });
  }

  // verify workflow
  const { workflow } = req.body;
  if (!isString(workflow)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'workflow',
        type: 'string',
      }),
    });
  }

  if (!workflow) {
    return res.status(400).json({
      error: res.tmf('validation_field_required', {
        field: 'workflow',
      }),
    });
  }

  if (!Permission.meta.workflow.enum.includes(workflow)) {
    return res.status(400).json({
      error: res.tmf('validation_field_value_out_of_range', {
        field: 'workflow',
        items: Permission.meta.workflow.enum.join(','),
      }),
    });
  }

  // verify default
  if (!isUndefined(req.body.default) && !isBoolean(req.body.default)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'default',
        type: 'boolean',
      }),
    });
  }

  // verify image
  if (!req.body.image) {
    return res.status(400).json({
      error: res.tmf('validation_field_required', {
        field: 'image',
      }),
    });
  }

  if (!isMongoId(req.body.image)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'image',
        type: 'ObjectId',
      }),
    });
  }

  const image = await Image.findById(req.body.image);
  if (!image) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'image',
      }),
    });
  }

  const permission = await Permission.create({
    abbreviation,
    name,
    description,
    scheduling,
    workflow,
    image: req.body.image,
    default: req.body.default,
  });

  if (permission.default) {
    await User.updateMany({}, { $push: { permissions: permission.id } });
  }

  return res.status(201).json({
    data: permission,
  });
});

exports.readOne = asyncMiddleware(async (req, res) => {
  const { permissionId } = req.params;

  if (!isMongoId(permissionId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const permission = await Permission.findById(permissionId);
  if (!permission) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'permission',
      }),
    });
  }

  return res.status(200).json({
    data: permission,
  });
});

exports.updateOne = asyncMiddleware(async (req, res) => {
  const { permissionId } = req.params;

  if (!isMongoId(permissionId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  // verify abbreviation
  if (req.body.abbreviation && !isString(req.body.abbreviation)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'abbreviation',
        type: 'string',
      }),
    });
  }

  const abbreviation = req.body.abbreviation.toUpperCase();
  if (abbreviation.length !== 3) {
    return res.status(400).json({
      error: res.tmf('validation_field_amount', {
        field: 'abbreviation',
        items: res.tn('%d character', 3),
      }),
    });
  }

  if (!isAlpha(abbreviation)) {
    return res.status(400).json({
      error: res.tmf('validation_field_characters_allowed', {
        field: 'abbreviation',
        items: res.tn('letters'),
      }),
    });
  }

  const abbreviationQuery = { _id: { $ne: permissionId }, abbreviation };
  const existingWithAbbreviation = await Permission.findOne(abbreviationQuery);
  if (existingWithAbbreviation) {
    return res.status(409).json({
      error: res.tmf('validation_entity_exists', {
        model: 'permission',
        uniqueProperties: 'abbreviation',
      }),
    });
  }

  // verify name
  if (req.body.name && !isString(req.body.name)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'name',
        type: 'string',
      }),
    });
  }

  const name = req.body.name.split(/\s+/g).join(' ');
  if (!isAlphanumeric(name.split(' ').join(''))) {
    return res.status(400).json({
      error: res.tmf('validation_field_characters_allowed', {
        field: 'name',
        items: res.t('letters, numbers and spaces'),
      }),
    });
  }

  const nameQuery = { _id: { $ne: permissionId }, name };
  const existingWithName = await Permission.findOne(nameQuery);
  if (existingWithName) {
    return res.status(409).json({
      error: res.tmf('validation_entity_exists', {
        model: 'permission',
        uniqueProperty: 'name',
      }),
    });
  }

  // verify description
  const { description } = req.body;
  if (description && !isString(description)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'description',
        type: 'string',
      }),
    });
  }

  // verify scheduling
  const { scheduling } = req.body;
  if (scheduling && !isString(scheduling)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'scheduling',
        type: 'string',
      }),
    });
  }

  if (!Permission.meta.scheduling.enum.includes(scheduling)) {
    return res.status(400).json({
      error: res.tmf('validation_field_value_out_of_range', {
        field: 'scheduling',
        items: Permission.meta.scheduling.enum.join(','),
      }),
    });
  }

  // verify workflow
  const { workflow } = req.body;
  if (workflow && !isString(workflow)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'workflow',
        type: 'string',
      }),
    });
  }

  if (!Permission.meta.workflow.enum.includes(workflow)) {
    return res.status(400).json({
      error: res.tmf('validation_field_value_out_of_range', {
        field: 'workflow',
        items: Permission.meta.workflow.enum.join(','),
      }),
    });
  }

  // verify default
  if (req.body.default && !isBoolean(req.body.default)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'default',
        type: 'boolean',
      }),
    });
  }

  // verify image
  if (req.body.image && !isMongoId(req.body.image)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'image',
        type: 'ObjectId',
      }),
    });
  }

  const image = await Image.findById(req.body.image);
  if (!image) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'image',
      }),
    });
  }

  const options = { new: true };
  const permission = await Permission.findByIdAndUpdate(
    permissionId,
    req.body,
    options
  );
  if (!permission) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'permission',
      }),
    });
  }

  return res.status(200).json({ data: permission });
});

exports.deleteOne = asyncMiddleware(async (req, res) => {
  const { permissionId } = req.params;

  if (!isMongoId(permissionId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const permission = await Permission.findByIdAndDelete(permissionId);
  if (!permission) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'permission',
      }),
    });
  }

  await User.updateMany({}, { $pull: { permissions: get(permission, '_id') } });

  return res.status(204).json(null);
});
