const mongoose = require('../../common/mongoose');
const generateMetaSchema = require('../../common/generateMetaSchema');

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const schedulings = ['on_demand', 'scheduled', 'locking'];
const defaultScheduling = schedulings[0];
const workflows = ['open', 'timed', 'controlled'];
const defaultWorkflow = workflows[0];

const permissionSchema = new Schema({
  abbreviation: {
    type: String,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
    unique: true,
  },
  description: {
    type: String,
    required: false,
    unique: false,
    default: '',
  },
  scheduling: {
    type: String,
    required: true,
    unique: false,
    default: defaultScheduling,
    enum: schedulings,
  },
  workflow: {
    type: String,
    required: true,
    unique: false,
    default: defaultWorkflow,
    enum: workflows,
  },
  default: {
    type: Boolean,
    required: true,
    unique: false,
    default: false,
  },
  image: {
    type: ObjectId,
    ref: 'images.file',
    required: true,
    unique: false,
  },
});

permissionSchema.statics.getDefaults = async function getDefaults() {
  const defaultPermissions = await this.find({ default: true }, { id: 1 });
  return defaultPermissions.map((permission) => permission.id);
};

permissionSchema.statics.meta = generateMetaSchema(permissionSchema);

module.exports = mongoose.model('permission', permissionSchema);
