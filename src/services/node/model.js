const mongoose = require('../../common/mongoose');
const generateMetaSchema = require('../../common/generateMetaSchema');

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const defaultOperatingSystem = 'linux';
const operatingSystems = ['linux', 'freertos', 'arduino'];
const roles = ['device_management', 'asset_management'];
const capabilities = ['raspberry_pi_camera'];
const populatable = ['sessions', 'machine'];

const nodeSchema = new Schema({
  serial_number: {
    type: String,
    required: true,
    unique: false,
  },
  mac_address: {
    type: String,
    required: true,
    unique: false,
  },
  operating_system: {
    type: String,
    required: true,
    unique: false,
    enum: operatingSystems,
    default: defaultOperatingSystem,
  },
  roles: [
    {
      type: String,
      required: true,
      unique: false,
      default: [],
      enum: roles,
    },
  ],
  capabilities: [
    {
      type: String,
      required: true,
      unique: false,
      default: [],
      enum: capabilities,
    },
  ],
  machine: {
    type: ObjectId,
    ref: 'machine',
    required: false,
    unique: false,
    default: null,
  },
  sessions: [
    {
      type: ObjectId,
      ref: 'session',
      required: true,
      unique: false,
      default: [],
    },
  ],
  release_channel: {
    type: String,
    required: false,
    default: 'master',
  },
  application_version: {
    type: String,
    required: false,
    default: '',
  },
  application_name: {
    type: String,
    required: false,
    default: '',
  },
});

nodeSchema.statics.meta = generateMetaSchema(nodeSchema);
nodeSchema.statics.populatable = populatable;

module.exports = mongoose.model('node', nodeSchema);
