const service = require('express').Router();
const {
  readNodeSchema,
  readSessionSchema,
  createOneNode,
  readManyNodes,
  readOneNode,
  updateOneNode,
  deleteOneNode,
  createOneNodeSession,
  patchOneNodeSession,
  readOneNodeSession,
} = require('./controllers');
const jwtAuthN = require('../../middlewares/authN/jwt');
const userRole = require('../../middlewares/authZ/userRole');
const session = require('../../middlewares/authZ/session');
const filterFields = require('../../middlewares/filterFields');
const requireFields = require('../../middlewares/requireFields');
const trimStrings = require('../../middlewares/trimStrings');

const isManager = userRole(['staff', 'admin']);

module.exports = () => {
  const modelFields = ['serial_number', 'mac_address', 'operating_system'];
  const optionalFields = [
    'release_channel',
    'application_version',
    'application_name',
  ];
  const nodeUpdate = [
    'roles',
    'capabilities',
    'release_channel',
    'application_version',
    'application_name',
  ];
  const update = ['authorized', 'identify'];

  // read many nodes
  service.get('/nodes', readManyNodes);

  // create one node
  service.post('/nodes', trimStrings);
  service.post('/nodes', filterFields([...modelFields, ...optionalFields]));
  service.post('/nodes', requireFields(modelFields));
  service.post('/nodes', createOneNode);

  // read node schema
  service.get('/nodes/schema', readNodeSchema);

  // read node schema
  service.get('/sessions/schema', readSessionSchema);

  // read one node
  service.get('/nodes/:nodeId', readOneNode);

  // update one node
  service.patch('/nodes/:nodeId', jwtAuthN);
  service.patch('/nodes/:nodeId', session(isManager));
  service.patch('/nodes/:nodeId', trimStrings);
  service.patch('/nodes/:nodeId', filterFields(nodeUpdate));
  service.patch('/nodes/:nodeId', updateOneNode);

  // delete one node
  service.delete('/nodes/:nodeId', jwtAuthN);
  service.delete('/nodes/:nodeId', isManager);
  service.delete('/nodes/:nodeId', deleteOneNode);

  // create one node session
  service.post('/nodes/:nodeId/sessions', trimStrings);
  service.post('/nodes/:nodeId/sessions', filterFields());
  service.post('/nodes/:nodeId/sessions', createOneNodeSession);

  // read one node session
  service.get('/nodes/:nodeId/sessions/:sessionId', readOneNodeSession);

  // update one node session
  service.patch('/nodes/:nodeId/sessions/:sessionId', jwtAuthN);
  service.patch('/nodes/:nodeId/sessions/:sessionId', isManager);
  service.patch('/nodes/:nodeId/sessions/:sessionId', trimStrings);
  service.patch('/nodes/:nodeId/sessions/:sessionId', filterFields(update));
  service.patch('/nodes/:nodeId/sessions/:sessionId', patchOneNodeSession);

  return service;
};
