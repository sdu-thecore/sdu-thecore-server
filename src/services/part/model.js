const mongoose = require('../../common/mongoose');

const { Schema } = mongoose;

const partSchema = new Schema({
  type: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('part', partSchema);
