const asyncMiddleware = require('express-async-handler');
const yup = require('yup');
const { stripLow, isHexadecimal, isMongoId } = require('validator');
const {
  BodyValidationError,
  ConflictError,
  EntityNotFoundError,
} = require('../../common/errors');
const readSchemaEndpoint = require('../../common/endpoints/readSchema');
const readManyEndpoint = require('../../common/endpoints/readMany');
const deleteOneEndpoint = require('../../common/endpoints/deleteOne');
const ToolType = require('../toolType/model');
const User = require('../user/model');
const Tool = require('./model');

const sanitize = (value) => (value ? stripLow(value, true) : value);
const validate = async (schema, payload) => {
  try {
    await schema.validate(payload, {
      strict: true,
      abortEarly: true,
      stripUnknown: true,
    });
  } catch (err) {
    if (err.name === 'ValidationError') {
      throw new BodyValidationError(err.errors.shift());
    }
    throw err;
  }
};

exports.readSchema = readSchemaEndpoint(Tool);

exports.readMany = readManyEndpoint(Tool);

exports.createOne = asyncMiddleware(async (req, res) => {
  const currentDate = new Date();
  const schema = yup
    .object({
      tag_id: yup
        .string()
        .ensure()
        .trim()
        .uppercase()
        .transform(sanitize)
        .test({
          name: 'hexadecimal',
          exclusive: false,
          message: res.tmf('validation_field_characters_allowed', {
            field: 'tag_id',
            items: res.tmf('hexadecimal characters'),
          }),
          test: (value) =>
            value === null || value === undefined || isHexadecimal(value),
        })
        .length(
          8,
          res.tmf('validation_field_amount', {
            field: 'tag_id',
            items: res.tn('%d character', 8),
          })
        )
        .required(
          res.tmf('validation_field_required', {
            field: 'tag_id',
          })
        ),
      size: yup
        .string()
        .ensure()
        .trim()
        .transform(sanitize)
        .max(
          50,
          res.tmf('validation_field_amount_max', {
            field: 'size',
            items: res.tn('%d character', 50),
          })
        )
        .notRequired()
        .default(''),
      shape: yup
        .string()
        .ensure()
        .trim()
        .transform(sanitize)
        .max(
          50,
          res.tmf('validation_field_amount_max', {
            field: 'shape',
            items: res.tn('%d character', 50),
          })
        )
        .notRequired()
        .default(''),
      type: yup
        .string()
        .ensure()
        .trim()
        .test({
          name: 'mongoId',
          exclusive: false,
          message: res.tmf('validation_field_type_invalid', {
            field: 'type',
            type: 'ObjectId',
          }),
          test: (value) =>
            value === null || value === undefined || isMongoId(value),
        })
        .nullable()
        .notRequired()
        .default(null),
      borrowed_by: yup
        .string()
        .ensure()
        .trim()
        .test({
          name: 'mongoId',
          exclusive: false,
          message: res.tmf('validation_field_type_invalid', {
            field: 'image',
            type: 'ObjectId',
          }),
          test: (value) =>
            value === null || value === undefined || isMongoId(value),
        })
        .nullable()
        .notRequired()
        .default(null),
      borrowed_at: yup
        .date()
        .max(
          currentDate,
          res.tmf('validation_field_time_no_future', {
            field: 'borrowed_at',
          })
        )
        .notRequired()
        .default(currentDate),
      available: yup.bool().notRequired().default(false),
    })
    .noUnknown();

  const payload = schema.cast(req.body);

  await validate(schema, payload);

  const existingWithTagId = await Tool.findOne({ tag_id: payload.tag_id });
  if (existingWithTagId) {
    throw new ConflictError(
      res.tmf('validation_entity_exists', {
        model: 'tool',
        uniqueProperties: 'tag_id',
      })
    );
  }

  if (payload.borrowed_by) {
    const user = await User.findById(payload.borrowed_by);
    if (!user) {
      throw new EntityNotFoundError(
        res.tmf('error_entity_not_found', {
          entity: 'user',
        })
      );
    }
    if (!payload.borrowed_since) {
      payload.borrowed_since = currentDate.toISOString();
    }
    payload.available = false;
  }

  if (payload.type) {
    const toolType = await ToolType.findById(payload.type);
    if (!toolType) {
      throw new EntityNotFoundError(
        res.tmf('error_entity_not_found', {
          entity: 'tool_type',
        })
      );
    }
  }

  if (!payload.type) {
    payload.borrowed_by = null;
    payload.available = false;
  }

  const tool = await Tool.create(payload);

  return res.status(201).json({
    data: tool,
  });
});

exports.readOne = asyncMiddleware(async (req, res) => {
  const { toolId } = req.params;

  if (isMongoId(toolId)) {
    const tool = await Tool.findById(toolId);
    if (tool) {
      return res.status(200).json({
        data: tool,
      });
    }
  }

  const tool = await Tool.findOne({ tag_id: toolId });
  if (!tool) {
    throw new EntityNotFoundError(
      res.tmf('error_entity_not_found', {
        entity: 'tool',
      })
    );
  }

  return res.status(200).json({
    data: tool,
  });
});

exports.updateOne = asyncMiddleware(async (req, res) => {
  const { toolId } = req.params;

  const currentDate = new Date();
  const schema = yup
    .object({
      tag_id: yup
        .string()
        .ensure()
        .trim()
        .uppercase()
        .transform(sanitize)
        .test({
          name: 'hexadecimal',
          exclusive: false,
          message: res.tmf('validation_field_characters_allowed', {
            field: 'tag_id',
            items: res.tmf('hexadecimal characters'),
          }),
          test: (value) =>
            value === null || value === undefined || isHexadecimal(value),
        })
        .length(
          8,
          res.tmf('validation_field_amount', {
            field: 'tag_id',
            items: res.tn('%d character', 8),
          })
        )
        .notRequired()
        .default(undefined),
      size: yup
        .string()
        .ensure()
        .trim()
        .transform(sanitize)
        .max(
          50,
          res.tmf('validation_field_amount_max', {
            field: 'size',
            items: res.tn('%d character', 50),
          })
        )
        .notRequired()
        .default(undefined),
      shape: yup
        .string()
        .ensure()
        .trim()
        .transform(sanitize)
        .max(
          50,
          res.tmf('validation_field_amount_max', {
            field: 'shape',
            items: res.tn('%d character', 50),
          })
        )
        .notRequired()
        .default(undefined),
      type: yup
        .string()
        .trim()
        .test({
          name: 'mongoId',
          exclusive: false,
          message: res.tmf('validation_field_type_invalid', {
            field: 'type',
            type: 'ObjectId',
          }),
          test: (value) =>
            value === null || value === undefined || isMongoId(value),
        })
        .nullable()
        .notRequired()
        .default(undefined),
      borrowed_by: yup
        .string()
        .trim()
        .test({
          name: 'mongoId',
          exclusive: false,
          message: res.tmf('validation_field_type_invalid', {
            field: 'borrowed_by',
            type: 'ObjectId',
          }),
          test: (value) =>
            value === null || value === undefined || isMongoId(value),
        })
        .nullable()
        .notRequired()
        .default(undefined),
      borrowed_at: yup
        .date()
        .max(
          currentDate,
          res.tmf('validation_field_time_no_future', {
            field: 'borrowed_at',
          })
        )
        .notRequired()
        .default(undefined),
      available: yup.bool().notRequired().default(false),
    })
    .noUnknown();

  const payload = schema.cast(req.body);

  await validate(schema, payload);

  if (payload.tag_id) {
    const tagIdQuery = { _id: { $ne: toolId }, tag_id: payload.tag_id };
    const existingtagId = await ToolType.findOne(tagIdQuery);
    if (existingtagId) {
      throw new ConflictError(
        res.tmf('validation_entity_exists', {
          model: 'tool',
          uniqueProperties: 'tag_id',
        })
      );
    }
  }

  if (payload.type) {
    const toolType = await ToolType.findById(payload.type);
    if (!toolType) {
      throw new EntityNotFoundError(
        res.tmf('error_entity_not_found', {
          entity: 'tool_type',
        })
      );
    }
  }

  const query = { $or: [{ _id: toolId }, { tag_id: toolId }] };
  const existingTool = await Tool.findOne(query);
  if (!existingTool) {
    throw new EntityNotFoundError(
      res.tmf('error_entity_not_found', {
        entity: 'tool',
      })
    );
  }

  // Check if the tool type will remain unchanged or if it will be assigned a value.
  if ((payload.type === undefined && existingTool.type) || payload.type) {
    // Check if the tool is being assigned to somebody else.
    if (payload.borrowed_by) {
      // Assign the tool if the user changed.
      if (existingTool.borrowed_by !== payload.borrowed_by) {
        const user = await User.findById(payload.borrowed_by);
        if (!user) {
          throw new EntityNotFoundError(
            res.tmf('error_entity_not_found', {
              entity: 'user',
            })
          );
        }
        if (!payload.borrowed_since) {
          payload.borrowed_since = currentDate.toISOString();
        }
        payload.available = false;
      }
    } else {
      payload.borrowed_by = null;
      payload.available = true;
    }
  } else {
    payload.borrowed_by = null;
    payload.available = false;
  }

  const options = { new: true };
  const tool = await Tool.findByIdAndUpdate(existingTool.id, payload, options);
  if (!tool) {
    throw new EntityNotFoundError(
      res.tmf('error_entity_not_found', {
        entity: 'tool',
      })
    );
  }

  return res.status(200).json({ data: tool });
});

exports.deleteOne = deleteOneEndpoint(Tool);
