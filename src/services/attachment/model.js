const { get, omit, isFunction, isObject } = require('lodash');
const mongoose = require('../../common/mongoose');
const { MONGODB_URL } = require('../../common/env');

const { MongoClient, GridFSBucket, ObjectId } = mongoose.mongo;
const model = 'attachments';
let client = null;

const normalize = (data) => {
  // check if data is an object
  if (!data || !isObject(data)) {
    return data;
  }
  // remove _id property
  const normalized = {
    ...omit(data, ['_id']),
  };
  // get _id property and attempt to cast it to a string
  const objectId = get(data, '_id');
  if (objectId && isFunction(objectId.toString)) {
    normalized.id = objectId.toString();
  }
  // return normalized object
  return normalized;
};

const getObjectId = (id) => (typeof id === 'object' ? id : ObjectId(id));

const connect = async () => {
  // wait for client connection to be ready
  if (!client) {
    client = await MongoClient.connect(MONGODB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  }
  // get database handle from current session or use test database
  const database = client.db(get(client, 's.options.dbName') || 'test');
  // configure GridFS bucket with name
  return new GridFSBucket(database, { bucketName: model });
};

const create = async (filename, metadata) => {
  const gfs = await connect();
  return gfs.openUploadStream(filename, metadata);
};

const find = async (ids) => {
  const gfs = await connect();
  const entities = await gfs
    .find({ _id: { $in: ids.map((id) => getObjectId(id)) } })
    .toArray();
  return entities.map(normalize);
};

const findById = async (id) => {
  const gfs = await connect();
  const entities = await gfs.find({ _id: getObjectId(id) }).toArray();
  return normalize(entities.shift());
};

const findByIdAndRead = async (id) => {
  const gfs = await connect();
  return gfs.openDownloadStream(getObjectId(id));
};

const findByIdAndDelete = async (id) => {
  const gfs = await connect();
  return gfs.delete(getObjectId(id));
};

module.exports = {
  connect,
  create,
  find,
  findById,
  findByIdAndRead,
  findByIdAndDelete,
};
