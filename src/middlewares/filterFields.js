const { isUndefined } = require('lodash');

module.exports = (fields = []) => (req, res, next) => {
  const props = Object.keys(req.body);
  const field = props.find(
    (prop) => !fields.includes(prop) && !isUndefined(req.body[prop])
  );
  if (field) {
    return res.status(400).json({
      error: res.tmf('validation_field_not_editable', {
        field,
      }),
    });
  }

  return next();
};
