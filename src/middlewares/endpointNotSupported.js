module.exports = (req, res) =>
  res.status(404).json({
    error: {
      type: 'EndpointNotFoundError',
      message: res.t('endpoint_not_supported'),
    },
  });
