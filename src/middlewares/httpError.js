const { HttpError } = require('../common/errors');

module.exports = (error, req, res, next) => {
  if (error instanceof HttpError) {
    return res.status(error.statusCode).json({
      error: {
        type: error.name,
        message: error.message,
      },
    });
  }

  return next(error);
};
