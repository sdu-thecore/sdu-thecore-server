const express = require('express');
const supertest = require('supertest');
const bodyParser = require('body-parser');
const filterFields = require('./filterFields');

const echoController = (req, res) => res.status(200).json(req.body);
const i18nMiddleware = (req, res, next) => {
  res.tmf = jest.fn((string) => string);
  res.t = jest.fn((string) => string);
  return next();
};

describe('filterFields middleware', () => {
  it('returns status code 400 and error message if body contains unknown fields', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(i18nMiddleware)
        .use(filterFields(['test']))
        .post('/', echoController);
      const body = {
        test: 'isValid',
        notTest: 'isInvalid',
      };
      const status = 400;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.status).toBe(status);
          expect(res.body).toEqual({
            error: 'validation_field_not_editable',
          });

          done(err);
        });
    });
  });

  it('calls next controller if body contains only allowed fields', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(i18nMiddleware)
        .use(filterFields(['test']))
        .post('/', echoController);
      const body = { test: 'a' };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.status).toBe(status);
          expect(res.body).toEqual(body);

          done(err);
        });
    });
  });
});
