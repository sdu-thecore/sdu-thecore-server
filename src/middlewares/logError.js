const logger = require('../common/logger');

module.exports = (err, req, res, next) => {
  logger.error(err.stack.replace(/\t/g, ''));
  return next(err);
};
