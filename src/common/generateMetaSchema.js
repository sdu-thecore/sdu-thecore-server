const { isFunction } = require('lodash');
const { walkValues } = require('./walk');

const getFunctionName = (value) => (isFunction(value) && value.name) || value;

module.exports = (schema) => walkValues(getFunctionName)(schema.obj);
