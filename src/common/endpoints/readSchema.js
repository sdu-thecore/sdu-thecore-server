const asyncMiddleware = require('express-async-handler');

module.exports = (Model) =>
  asyncMiddleware(async (req, res) =>
    res.status(200).json({ data: Model.meta })
  );
