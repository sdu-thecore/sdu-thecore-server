const asyncMiddleware = require('express-async-handler');
const { isMongoId } = require('validator');
const { camelCase } = require('lodash');
const {
  ParameterValidationError,
  EntityNotFoundError,
  QueryValidationError,
} = require('../errors');

module.exports = (Model) =>
  asyncMiddleware(async (req, res) => {
    const idName = `${camelCase(Model.modelName)}Id`;
    const id = req.params[idName];

    if (!isMongoId(id)) {
      throw new ParameterValidationError(
        res.tmf('validation_param_type_invalid', {
          type: 'ObjectId',
        })
      );
    }

    const populate = {
      path: '',
    };
    if (req.query.populate && Model.populatable) {
      const fields = req.query.populate.split(',');
      if (fields.find((field) => !Model.populatable.includes(field))) {
        throw new QueryValidationError(
          res.tmf('validation_query_value_out_of_range_multiple', {
            field: 'populate',
            items: Model.populatable.join(','),
          })
        );
      }
      populate.path = fields.join(' ');
    }

    const toolType = await Model.findById(id).populate(populate);
    if (!toolType) {
      throw new EntityNotFoundError(
        res.tmf('error_entity_not_found', {
          entity: 'tool_type',
        })
      );
    }

    return res.status(200).json({
      data: toolType,
    });
  });
