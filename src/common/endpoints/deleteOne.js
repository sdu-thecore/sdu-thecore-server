const asyncMiddleware = require('express-async-handler');
const { isMongoId } = require('validator');
const { camelCase } = require('lodash');
const { ParameterValidationError, EntityNotFoundError } = require('../errors');

module.exports = (Model) =>
  asyncMiddleware(async (req, res) => {
    const idName = `${camelCase(Model.modelName)}Id`;
    const id = req.params[idName];

    if (!isMongoId(id)) {
      throw new ParameterValidationError(
        res.tmf('validation_param_type_invalid', {
          type: 'ObjectId',
        })
      );
    }

    const entity = await Model.findByIdAndDelete(id);
    if (!entity) {
      throw new EntityNotFoundError(
        res.tmf('error_entity_not_found', {
          type: 'ObjectId',
        })
      );
    }

    return res.status(204).json(null);
  });
