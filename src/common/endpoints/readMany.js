const asyncMiddleware = require('express-async-handler');
const { QueryValidationError } = require('../errors');

module.exports = (Model) =>
  asyncMiddleware(async (req, res) => {
    const limit = parseInt(req.query.limit, 10) || 100;
    const offset = parseInt(req.query.offset, 10) || 0;
    const sortOrder = req.query.sort_order || 'asc';
    const sortBy = req.query.sort_by || 'created';
    const allowedSortOrder = ['asc', 'desc'];

    if (sortBy && Array.isArray(sortBy)) {
      throw new QueryValidationError(
        res.tmf('validation_query_field_not_unique', {
          field: 'sort_by',
        })
      );
    }

    if (sortOrder && Array.isArray(sortOrder)) {
      throw new QueryValidationError(
        res.tmf('validation_query_field_not_unique', {
          field: 'sort_order',
        })
      );
    }

    if (sortOrder && !allowedSortOrder.includes(sortOrder)) {
      throw new QueryValidationError(
        res.tmf('validation_query_value_out_of_range', {
          field: 'sort_order',
          items: allowedSortOrder.join(', '),
        })
      );
    }

    if (sortBy && !Model.sortable.includes(sortBy)) {
      throw new QueryValidationError(
        res.tmf('validation_query_value_out_of_range', {
          field: 'sort_by',
          items: Model.sortable.join(', '),
        })
      );
    }

    const populate = {
      path: '',
    };
    if (req.query.populate && Model.populatable) {
      const fields = req.query.populate.split(',');
      if (fields.find((field) => !Model.populatable.includes(field))) {
        throw new QueryValidationError(
          res.tmf('validation_query_value_out_of_range_multiple', {
            field: 'populate',
            items: Model.populatable.join(','),
          })
        );
      }
      populate.path = fields.join(' ');
    }

    const options = { sort: { [sortBy]: sortOrder }, offset, limit, populate };
    const { docs, totalDocs, ...metadata } = await Model.paginate({}, options);
    return res.status(200).json({
      data: docs,
      count: totalDocs,
      ...metadata,
    });
  });
