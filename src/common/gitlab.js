const axios = require('axios');
const { GITLAB_API_TOKEN } = require('./env');

module.exports = axios.create({
  baseURL: 'https://gitlab.com/api/v4',
  headers: {
    'PRIVATE-TOKEN': GITLAB_API_TOKEN,
  },
});
