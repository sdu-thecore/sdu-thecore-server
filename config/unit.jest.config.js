module.exports = {
  rootDir: '../',
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.js', '!src/index.js'],
  coverageReporters: ['text'],
  testEnvironment: 'node',
};
